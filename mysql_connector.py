import mysql.connector
import sys

#DB variables
user_n = 'filipe'
password_u = 'secret'
host_db = '34.240.112.214'
database_use = 'pubs'

#The db
try:
    db_connection = mysql.connector.connect(password=password_u, user=user_n, host=host_db, database=database_use)
except:
    print('Could not connect to DB. Please check your code & connection')
    sys.exit(1)

# The mouse to click stuff
dbcur = db_connection.cursor(dictionary=True)

# using the cursor to execute some sql
dbcur.execute("SELECT * FROM publishers;")

print(dbcur)

# Getting all the records could be to heavy
# could freeze system

for record in dbcur:
    # print(type(record))
    #print(record)
    print(record['pub_name'], 'at:', record['city'] )

db_connection.close()




