import mysql_conn_oop
#import pdb; pdb.set_trace()

class msfilm_db(mysql_conn_oop.mysql_conn):
    # use Inheritance polymorphism to change the connection to be for msfilm
    def __init__(self, ip='34.240.112.214', password='secret', user_n='filipe', database='msFilm'):
        super().__init__(ip, password, user_n, database)

    # CRUD

    # READ all films
    def movies(self):
        self.dbcur.execute('SELECT * FROM Film;')
        return self.dbcur

    def print_movies(self):
        return self.dbcur.execute('SELECT * FROM Film;')


    # Search for film name
    def get_film(self, film_name):
        #What happens if it matches more than one movie?
        self.dbcur.execute(f"SELECT * FROM Film WHERE Title LIKE '%{film_name}%';")
        return self.dbcur.fetchone()


    # Create one film




example_db = msfilm_db()
print(example_db.database_use)

example_db.dbcur.execute("SELECT * FROM Film;")


for record in example_db.dbcur:
    print(record)

print('////////////////////')
all_movies = example_db.movies()

for movie in all_movies:
    print(movie['Title'])


print("/////")
print(example_db.get_film('Empire'))
print(example_db.get_film('UP'))



#print(example_db.database_use)
#all_film = example_db.movies()
#print(all_film)

#for record in all_film:
#    print(record)
