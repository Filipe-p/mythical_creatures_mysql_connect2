# Mythical Creature Connect DB to Python Project

We have created a DB for our mythical creature. 
There are 3 tables: 
- Users
- Creatures
- Bookings

Please see the following user stories and details on how to run the project.
Also read on for details of what the class will cover. 

## User Stories (Scrum, Part of Agile)

[CORE-User stories] AS a user I want to be able to add an exotic/Mythical creature to be a companion, so that it can be petted or go on an adventure.

[CORE-User stories] AS a user I want to be able to specify creturePET_Name, Creature_type, Creture_science_name, abilities, temper, age, emergency_contact. 

[CORE-User stories] AS a user I want to be able to book a creature for an adventure. 

[User stories] AS a user I want to be able search for all creatures, so I can browse.

[User stories] AS a user I want to be able to search for all creatures base on creature type, so i can browse only a type of creature.

[User stories] AS a user I want to be able to search for all creatures base on creature_Pet_name, so i can find a creature.

## Class Plan 

This class is for us to connect python to SQL. 

Once we have done that we will start abstracting our interaction with the DB. 
--> My lazy objective is that I don't have to write SQL any more :) 

We'll follow the CRUD desing principle
- Create
- Read
  - Read one
  - Read all (with filter)
- Update 
- Delete

If your application CRUDS then 80% of main functionality is done.


## Computer Science Concepts 

- OOP 
- Abstraction 
- Seperation of Concerns ()


## OOP VS Function 

OOP is object orientated and is designed around object.
Classes are blue prints of object and the actions (methods) available for said object.

Functions are anonymous, do not requiere an object to be called on. 

### CLASS

A class is a wrapper and a blue print for method os an object. 
The init class is the blue print of how to construct an object (cookie cutter).
And the then is can hold more methods, that can use and be use by the self (the cookie).

Syntax 
```Python
class NameClass()
  def __init__(self, arg1, arg2, arg3):
    self.parameter_of_object1 = arg1
    self.parameter_of_object2 = arg2
    self.parameter_of_object3 = arg3
  
```

### METHOD 
Is a block of code inside class, that can be called on an object of said class (things a cookie can do).

For example a cookie might be able to:
 - attrack_hungry_person()
 - sit_in_jar()
 - rot_decay()

```Python
class CookieCLass()
  def __init__(self, arg1, arg2, arg3):
    self.best_visual_qualites = arg1
    self.best_aroma_qualities = arg2
    self.parameter_of_object3 = arg3
  
  def attrack_hungry_person(self):
    print('look at these    ----->')
    print(self.best_visual_qualites)
    print("Can't resist the freshness")
    print(self.best_aroma_qualities)
    


### SELF 

### 4 Pillars, Inheritance and Super
