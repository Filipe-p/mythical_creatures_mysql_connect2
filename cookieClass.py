class CookieCLass():
  def __init__(self, look, smell, arg3):
    self.best_visual_qualites = look
    self.best_aroma_qualities = smell
    self.parameter_of_object3 = arg3
    self.days_old = 0
  
  def attrack_hungry_person(self):
    print('look at these    ----->')
    print(self.best_visual_qualites)
    print("Can't resist the freshness")
    print(self.best_aroma_qualities)
    self.__age_cookie()

  #encapsulated method - only accessed from within. It's a private method
  def __age_cookie(self):
    self.days_old += 1



# create a new cookie
cookie1 = CookieCLass('with chocolate chips', 'very buttery', 'chunky')
# check it's age
print("age in days:", cookie1.days_old)

# run the method attrack_hungry_person
cookie1.attrack_hungry_person()
cookie1.attrack_hungry_person()
cookie1.attrack_hungry_person()

# check it's age
print("age in days:", cookie1.days_old)













#chocolate_cookie = CookieCLass('Amazing with chocolate', 'Even Better', 'Chunky')
#chocolate_cookie2 = CookieCLass('Amazing with lots of chocolate', 'Even Better', 'Chunky')
#vanilla_cookie1 = CookieCLass('Amazing, but a bit bland', 'smell very sweat', 'Chunky')
#vanilla_cookie2 = CookieCLass('Amazing with vanilla', 'very sweat', 'Chunky')

#print(chocolate_cookie.attrack_hungry_person())
#print(chocolate_cookie2.attrack_hungry_person())
#print(vanilla_cookie2.attrack_hungry_person())


