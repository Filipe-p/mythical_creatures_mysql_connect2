# Importing a package (means it's maintained by a third party - not python org)
import mysql.connector
# Importing standards library - maintained by Python org
import sys

# OOP Class for a general connection to the SQL db

class mysql_conn():

    # Defaults allow you to set default data
    def __init__(self, ip='34.240.112.214', password='secret', user_n='filipe', database='pubs'):
        self.user_n = user_n
        self.password_u = password
        self.host_db = ip
        self.database_use = database
        scoped_variable = 'inside the method'
        # self.__makeconnection()

        try:
            self.db_connection = mysql.connector.connect(password=self.password_u, user=self.user_n,
                                                             host=self.host_db, database=self.database_use)
        except:
            print('Could not connect to DB. Please check your code & connection')
            sys.exit(1)

        self.dbcur = self.db_connection.cursor(dictionary=True)


#    def __makeconnection(self): ## encapsulated method - Private method that can only be called within a class
#        try:
#            self.db_connection = mysql.connector.connect(password=self.password_u, user=self.user_n, host=self.host_db, database=self.database_use)
#        except:
#            print('Could not connect to DB. Please check your code & connection')
#            sys.exit(1)
#
#        self.dbcur = db_connection.cursor(dictionary=True)


